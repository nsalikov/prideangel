# Prideangel Parser

## Getting Started

### Prerequisites

* [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
* [python 3](https://docs.python.org/3/using/index.html)

### Installing

Clone the project:

```
git clone https://nsalikov@bitbucket.org/nsalikov/prideangel.git
```

Change directory and install requirements:

```
cd prideangel
pip install -r requirements.txt
```

If you are using Anaconda/Conda, add `conda-forge` channel to your channels first (than is required for  `dateparser` library):

```
conda config --add channels conda-forge
cd prideangel
conda install --file requirements.txt
```

### Configuration

Set up these variables in `settings.py`:

```
DOWNLOAD_DELAY = 1

LOGIN = 'login'
PASSWORD = 'password'

DEFAULT_SEARCH_PARAMS_FILE = 'default_params.txt'
DEFAULT_SEARCH_PARAMS_FILE = os.path.join(CWD, '..', DEFAULT_SEARCH_PARAMS_FILE)

DEFAULT_BLACKLIST_FILE = 'blacklist.txt'
DEFAULT_BLACKLIST_FILE = os.path.join(CWD, '..', DEFAULT_BLACKLIST_FILE)

DEFAULT_LIKE_FILE = 'likes.txt'
DEFAULT_LIKE_FILE = os.path.join(CWD, '..', DEFAULT_LIKE_FILE)

IMAGES_STORE = 'images'
IMAGES_STORE = os.path.join(CWD, '..', IMAGES_STORE)

```

Here you can find values for all parameters for `DEFAULT_SEARCH_PARAMS_FILE`:

```
https://www.prideangel.com/Members/Members-Directory/Advanced-Search.aspx
```

Check `div#p_lt_ctl06_pageplaceholder_p_lt_ctl00_fltMbrs_filterControl_pnlFilter` element in browser DevTools.

Also, you can add ids into `DEFAULT_BLACKLIST_FILE` and `DEFAULT_BLACKLIST_FILE` files. One id per line.

## Usage

Just run:

```
scrapy crawl search
```
Or:

```
scrapy crawl like
```

You can specify output files for data and/or logs:

```
scrapy crawl search --output=search1.jl --logfile=search1.log
```

NOTE: extension matters in case of `output` parameter. Always use `.jl`.

Finally, you can specify different files for search parameters, blacklist and likes:

```
scrapy crawl search --a params=srecipient.txt --a blacklist=another_blacklist.txt --output=srecipient.jl
scrapy crawl like --a lst=another_likes_file.txt --output=likes.jl
```

## Scrapy Documentation

Want to know more? Check Scrapy [official docs](https://docs.scrapy.org/en/1.5/).
