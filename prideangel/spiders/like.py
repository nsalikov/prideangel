# -*- coding: utf-8 -*-
import re
import json
import scrapy
from scrapy.exceptions import CloseSpider
from scrapy.shell import inspect_response


class LikeSpider(scrapy.Spider):
    name = 'like'
    allowed_domains = ['prideangel.com']
    profile_url = 'https://www.prideangel.com/Members/Members-Directory/View/{}.aspx'
    login_url = 'https://www.prideangel.com/Special-Pages/Login.aspx'
    like_url = 'https://www.prideangel.com/CMSModules/PA/CMSPages/LikeFavouriteAJAX.aspx'

    custom_settings = {
        'DOWNLOAD_DELAY': 1,
        'ITEM_PIPELINES': {},
    }


    def start_requests(self):
        self.login = self.settings.get('LOGIN')
        self.password = self.settings.get('PASSWORD')

        self.headers = self.settings.get('DEFAULT_REQUEST_HEADERS')
        self.headers['User-Agent'] = self.settings.get('USER_AGENT')

        self.headers['Accept'] = '*/*'
        self.headers['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8'
        self.headers['Origin'] = 'https://www.prideangel.com'
        self.headers['Referer'] = 'https://www.prideangel.com/Members/Members-Directory.aspx'
        self.headers['X-Requested-With'] = 'XMLHttpRequest'

        self._lst = []

        if not hasattr(self, 'lst'):
            self.lst = self.settings.get('DEFAULT_LIKE_FILE')

        if self.lst:
            with open(self.lst) as lst_file:
                for line in lst_file:
                    self._lst.append(line.strip())

        if self._lst:
            yield scrapy.Request(self.login_url, callback=self.do_login, dont_filter=True)
        else:
            raise CloseSpider("User list is empty")


    def do_login(self, response):

        formdata = {
                    'p$lt$ctl06$pageplaceholder$p$lt$ctl00$LF$Login1$UserName': self.login,
                    'p$lt$ctl06$pageplaceholder$p$lt$ctl00$LF$Login1$Password': self.password,
                }

        r = scrapy.FormRequest.from_response(response, formdata=formdata, callback=self.check_login)

        return r


    def check_login(self, response):
        # inspect_response(response, self)

        if response.css('li.current-user').extract_first():
            return self.after_login(response)
        else:
            return self.login_fail(response)


    def login_fail(self, response):
        # self.logger.critical("Unable to login")
        raise CloseSpider("Something wrong. Unable to login.")


    def after_login(self, response):
        return scrapy.Request('https://www.prideangel.com/Members/Members-Directory.aspx', callback=self.check_profiles)


    def check_profiles(self, response):
        for _id in self._lst:
            url = self.profile_url.format(_id)
            meta = {'id': _id}
            yield scrapy.Request(url, meta=meta, callback=self.parse_profile)


    def parse_profile(self, response):
        _id = response.meta['id']

        if response.css('a#p_lt_ctl06_pageplaceholder_p_lt_ctl02_ViewProfile_Header_btnLike.active'):
            self.logger.debug("Already liked: {}".format(_id))
            return

        body = 'uid={}&like=true'.format(_id)
        meta = {'id': _id}
        yield scrapy.Request(self.like_url, method='POST', headers=self.headers, body=body, meta=meta, callback=self.parse, dont_filter=True)


    def parse(self, response):
        _id = response.meta['id']

        try:
            data = json.loads(response.text)
            data['id'] = _id
            return data
        except ValueError as e:
            self.logger.warning("Unable to parse response: {}\nUser ID: {}\nResponse: {}".format(e, _id, response.text))
