# -*- coding: utf-8 -*-
import re
import json
import scrapy
import dateparser
from datetime import datetime
from scrapy.exceptions import CloseSpider
from scrapy.shell import inspect_response


class SearchSpider(scrapy.Spider):
    name = 'search'
    allowed_domains = ['prideangel.com']
    login_url = 'https://www.prideangel.com/Special-Pages/Login.aspx'
    search_url = 'https://www.prideangel.com/Members/Members-Directory/Advanced-Search.aspx'


    def start_requests(self):
        self.date = datetime.now().isoformat()

        self.login = self.settings.get('LOGIN')
        self.password = self.settings.get('PASSWORD')

        if not hasattr(self, 'params'):
            self.params = self.settings.get('DEFAULT_SEARCH_PARAMS_FILE')

        with open(self.params) as json_file:
            try:
                self._params = json.load(json_file)
            except ValueError as e:
                raise CloseSpider("Unable to parse json file: {}".format(e))

        self._blacklist = []

        if not hasattr(self, 'blacklist'):
            self.blacklist = self.settings.get('DEFAULT_BLACKLIST_FILE', None)

        if self.blacklist:
            with open(self.blacklist) as blacklist_file:
                for line in blacklist_file:
                    self._blacklist.append(line.strip())

        yield scrapy.Request(self.login_url, callback=self.do_login, dont_filter=True)


    def do_login(self, response):

        formdata = {
                    'p$lt$ctl06$pageplaceholder$p$lt$ctl00$LF$Login1$UserName': self.login,
                    'p$lt$ctl06$pageplaceholder$p$lt$ctl00$LF$Login1$Password': self.password,
                }

        r = scrapy.FormRequest.from_response(response, formdata=formdata, callback=self.check_login)

        return r


    def check_login(self, response):
        # inspect_response(response, self)

        if response.css('li.current-user').extract_first():
            return self.after_login(response)
        else:
            return self.login_fail(response)


    def login_fail(self, response):
        # self.logger.critical("Unable to login")
        raise CloseSpider("Something wrong. Unable to login.")


    def after_login(self, response):
        return scrapy.Request(self.search_url, callback=self.do_search)


    def do_search(self, response):
        r = scrapy.FormRequest.from_response(response, formdata=self._params, callback=self.parse_search)

        return r


    def parse_search(self, response):
        items = response.css('div.user-item a.lnk-wrp ::attr(href)').extract()
        for item in items:
            match = re.search('View/(\d+)\.aspx', item)

            if not match:
                self.logger.warning("[parse_search] Unable to get user ID from url: {}".format(item))
                continue

            user_id = match.group(1)

            if user_id not in self._blacklist:
                yield response.follow(item, callback=self.parse_profile)

        pages = response.css('div.pagination a ::attr(href)').extract()
        for page in pages:
            yield response.follow(page, callback=self.parse_search)


    def parse_profile(self, response):
        # inspect_response(response, self)

        d = {}

        d['scraping_date'] = self.date

        match = re.search('View/(\d+)\.aspx', response.url)
        if not match:
            self.logger.warning("[parse_profile] Unable to get user ID from url: {}".format(response.url))
            return

        d['id'] = match.group(1)

        name = ' '.join(response.css('h1 ::text').extract())
        name = re.sub('[\t|\r|\n|\xa0]+', ' ', name)
        name = re.sub('\s{2,}', ' ', name)

        d['url'] = response.url

        d['name'] = _strip(name)
        d['location'] = _strip(response.css('#p_lt_ctl06_pageplaceholder_p_lt_ctl02_ViewProfile_Header_lblAdditionalDetail > p > span:nth-of-type(1) ::text').extract_first())
        d['type'] = _strip(response.css('#p_lt_ctl06_pageplaceholder_p_lt_ctl02_ViewProfile_Header_lblAdditionalDetail > p > span:nth-of-type(2) ::text').extract_first())

        registered = ' '.join(response.css('#p_lt_ctl06_pageplaceholder_p_lt_ctl01_ViewProfile_LeftColumn_lblRegDate ::text').extract())
        registered = re.sub('[\t|\r|\n|\xa0]+', ' ', registered)
        registered = re.sub('\s{2,}', ' ', registered)
        registered = re.sub('\s*Registered on:\s*', '', registered)

        try:
            d['registered'] = dateparser.parse(registered).date().isoformat()
        except:
            d['registered'] = None

        if response.css('#p_lt_ctl06_pageplaceholder_p_lt_ctl01_ViewProfile_LeftColumn_pnlMemberLevel span.ico-premium').extract_first():
            d['is_premium'] = 'yes'
        else:
            d['is_premium'] = 'no'

        d['last_login_text'] = _strip(response.css('div.last-login span:nth-of-type(2) ::text').extract_first())

        try:
            d['last_login_date'] = dateparser.parse(d['last_login_text']).date().isoformat()
        except:
            d['last_login_date'] = None

        d['about'] = response.css('#p_lt_ctl06_pageplaceholder_p_lt_ctl02_ViewProfile_Information_lblAbout ::text').extract_first()
        d['interests'] = response.css('#p_lt_ctl06_pageplaceholder_p_lt_ctl02_ViewProfile_Information_lblInterests ::text').extract_first()

        details = [text.strip() for text in response.css('#p_lt_ctl06_pageplaceholder_p_lt_ctl02_ViewProfile_Information_pnlDetails div.col-md-6 div span ::text').extract()]
        it = iter(details)

        d['details'] = {}

        for k,v in zip(it,it):
            d['details'][k] = v

        d['image_urls'] = response.css('a#p_lt_ctl06_pageplaceholder_p_lt_ctl01_ViewProfile_LeftColumn_imgProfilePicture ::attr(href)').extract()
        d['image_urls'] = [url for url in d['image_urls'] if 'prideangel-default-image.aspx' not in url]

        gallery = response.css('#p_lt_ctl06_pageplaceholder_p_lt_ctl01_ViewProfile_LeftColumn_pnlGallery a ::attr(href)').extract()

        d['image_urls'] = d['image_urls'] + gallery
        d['image_urls'] = [add_domain('https://www.prideangel.com', url) for url in d['image_urls']]

        return d


def _strip(string, default=None):
    if string:
        return string.strip()
    return default


def add_domain(domain, url):
    if not url.startswith('http'):
        return domain + url
    else:
        return url
